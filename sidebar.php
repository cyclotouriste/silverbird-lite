<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="col-md-4 col-sm-4" <?php if( get_theme_mod('silverbird_sticky_sidebar', false ) == true ) { echo esc_attr('data-sticky_column'); } ?>>
	<aside id="secondary" class="sidebar">
		<?php
			if ( silverbird_live_preview() ) {
				get_template_part( 'core/preview/cat-posts' );
			}
		?>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside><!-- #secondary -->
</div>
