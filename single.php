<?php
/**
 * The template for displaying all single posts
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

get_header();

?>

	<div class="main-content">
        <div class="container">
            <div class="row">
            	<div class="col-md-8 col-sm-8">
            		<?php
            		while ( have_posts() ) : the_post();
            			get_template_part( 'template-parts/content', 'single' ); ?>
            		<?php /* Box to display information about the post Author */
                        silverbird_author_box(); ?>
                       <?php
                        /* Call Posts Navigation */
                       silverbird_posts_navigation();?>
                		<?php
                        /* If related posts is active, get it */
                        get_template_part( 'template-parts/related', 'posts' );

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                        	comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>
        		</div><!-- .col-md-8 -->

        		<?php
                    get_sidebar();
                ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .main-content -->
<?php
get_footer();
