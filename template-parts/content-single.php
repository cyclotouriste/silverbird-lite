<?php
/**
 * Template part for displaying posts
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'single-content' ); ?>>
	<header class="entry-header text-center">
		<?php if(get_theme_mod('silverbird_single_cats', true ) == true ){ ?>
            <div class="entry-cat">
                <?php silverbird_category_list(); ?>
            </div>
        <?php } ?>
        <?php the_title( '<h1 class="entry-title">', '</h1>' );?>
        <?php if ( 'post' === get_post_type() ) : ?>
        <div class="entry-meta">
            <?php if(get_theme_mod('silverbird_single_date', true ) == true ){ ?><span class="date"> <?php echo esc_attr( get_the_date() ) ; ?></span><?php } ?>

            <?php if(get_theme_mod('silverbird_single_comments_info', true ) == true ){ ?><span class="comment"> <?php comments_popup_link( '0 Comment', '1 Comment', '% Comments', 'comments-link', 'Comments are off'); ?></span>
            <?php } ?>
         </div>
         <?php endif; ?>
    </header>

	<?php silverbird_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'silverbird' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'silverbird' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			silverbird_tag_list();
			silverbird_entry_footer();
		?>
	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->
