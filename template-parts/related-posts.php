<?php
/**
 * Template part for displaying related posts
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

if( get_theme_mod('silverbird_single_related_posts', true ) == true ) {

	$related = silverbird_related_posts();
?>

<?php if ( $related->have_posts() ): ?>
<?php $relattitle = get_theme_mod('silverbird_single_related_title', 'You might also like' ); ?>
	<div class="related-post text-center"><!--related post carousel-->
	    <div class="related-heading">
	        <h4 class="text-left"><?php printf( esc_attr__( '%s ', 'silverbird' ), $relattitle ); ?></h4>
	    </div>
	    <div class="related-items">
	        <?php while( $related->have_posts() ) { $related->the_post(); ?>
	            <div class="items">
	                <a href="<?php echo esc_url( get_permalink() ); ?>">
	                    <?php if (has_post_thumbnail()) { ?>
	                        <img src="<?php the_post_thumbnail_url('silverbird-related') ; ?>" alt="<?php the_title_attribute();?>">
	                    <?php } else { ?>
	                        <img src="<?php echo esc_url(SILVERBIRD_DIRECTORY_URI); ?>/assets/images/related.jpg" alt="<?php the_title_attribute();?>">
	                    <?php }?>
	                        <h5 class="text-center"><?php the_title(); ?></h5>
	                </a>
	            </div>
	        <?php } ?>
	        <?php wp_reset_postdata(); ?>
	    </div><!--related-items-->
	</div><!--related post carousel-->

<?php
	endif; //end if $related->have_posts()

	wp_reset_query();

}
?>
