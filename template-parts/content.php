<?php
/**
 * Template part for displaying posts
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post blockview'); ?>>
    <?php if(get_theme_mod('silverbird_posts_cats', true ) == true ){ ?>
        <div class="entry-cat text-center">
            <?php silverbird_category_list(); ?>
        </div>
    <?php } ?>
    <header class="entry-header text-center">
        <?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
        <?php if ( 'post' === get_post_type() ) : ?>
        <div class="entry-meta">
            <?php if(get_theme_mod('silverbird_posts_date', true ) == true ){ ?><span class="date"> <?php echo esc_attr( get_the_date() ) ; ?></span><?php } ?>

            <?php if(get_theme_mod('silverbird_posts_comment', true ) == true ){ ?><span class="comment"><?php comments_popup_link( '0 Comment', '1 Comment', '% Comments', 'comments-link', 'Comments are off'); ?></span>
            <?php } ?>
         </div>
         <?php endif; ?>
    </header>

    <div class="entry-content">
        <p><?php the_excerpt(); ?></p>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
