<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

get_header(); ?>

	<div class="p-404">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="primary" class="content-area padding-content white-color">
                        <main id="main" class="site-main" role="main">

                            <section class="error-404 not-found text-center">
                                <h1 class="404"><?php esc_html_e('404','silverbird'); ?></h1>

                                <p class="lead"><?php esc_html_e('Sorry, we could not find the page you are looking for!','silverbird'); ?></p>

                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-4">
                                        <form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                            <div>
                                                <input type="text" placeholder="<?php esc_attr_e('Search and hit enter...','silverbird'); ?>" name="s"
                                                       id="s"/>
                                            </div>
                                        </form>
                                        <p class="go-back-home"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                            <?php esc_html_e('Back to Home Page','silverbird'); ?></a></p>
                                    </div>
                                </div>

                            </section><!-- .error-404 -->

                        </main><!-- #main -->
                    </div><!-- #primary -->
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
