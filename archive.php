<?php
/**
 * The template for displaying archive pages
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

get_header(); ?>

	<div class="main-content">
        <div class="container">
            <div class="row">
            	<div class="col-md-8 col-sm-8">

				<?php
				if ( have_posts() ) : ?>

					<header class="page-header">
						<?php
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
						?>
					</header><!-- .page-header -->

					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						//get_template_part( 'template-parts/content', get_post_format() );

						if( get_theme_mod('silverbird_posts_layout', 'blockview' ) == 'blockview' ){
							get_template_part( 'template-parts/content', 'blockview' );
						}elseif( get_theme_mod('silverbird_posts_layout', 'blockview' ) == 'listview' ){
							get_template_part( 'template-parts/content', 'listview' );
						}

					endwhile;

					the_posts_pagination(
					    array(
		                    'mid_size' => 3,
		                    'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i> ',
		                    'next_text' => ' <i class="fa fa-arrow-right" aria-hidden="true"></i> ',
		                )
		            );

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>

				</div><!-- .col-md-8-->
				<?php
					get_sidebar();
				?>

			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .main-content -->

<?php
get_footer();
