<?php
/**
 * The template for displaying Woocommerce pages
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

get_header();

?>

	<div class="main-content">
        <div class="container">
            <div class="row">
            	<div class="col-md-8 col-sm-8">
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
						<?php
							woocommerce_content();
						?>
					</article>
				</div><!-- .col-md-8 -->

				<div class="col-md-4 col-sm-4">
					<aside id="secondary" class="sidebar">
						<?php
							 if ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
						 	// Sidebar for Woocommerce if installed
				            if ( is_active_sidebar( 'woocommerce-sidebar' ) ) {
				                dynamic_sidebar( 'woocommerce-sidebar' );
				        	}
				    	}
						?>
					</aside>
				</div>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .main-content -->

<?php
get_footer();
