<?php
/**
 * Silverbird Lite functions and definitions
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */


define ('SILVERBIRD_DIRECTORY_URI', get_template_directory_uri() );
define ('SILVERBIRD_DIRECTORY', get_template_directory() );
define ('SILVERBIRD_FUNCTIONS_DIRECTORY', SILVERBIRD_DIRECTORY . '/core' );

/**
 * Customizer additions.
 */
require SILVERBIRD_FUNCTIONS_DIRECTORY . '/customizer/kirki-fallback.php';
require SILVERBIRD_FUNCTIONS_DIRECTORY . '/customizer/customizer.php';
require SILVERBIRD_FUNCTIONS_DIRECTORY . '/customizer/panels.php';
require SILVERBIRD_FUNCTIONS_DIRECTORY . '/customizer/sections.php';
require SILVERBIRD_FUNCTIONS_DIRECTORY . '/customizer/fields.php';
require SILVERBIRD_FUNCTIONS_DIRECTORY . '/customizer/custom-css.php';


/**
 * Include recommended plugins.
 */
include( SILVERBIRD_FUNCTIONS_DIRECTORY . '/admin/tgm/tgm-init.php' );
/**
 * Theme scripts.
 */
require SILVERBIRD_FUNCTIONS_DIRECTORY . '/scripts.php';
/**
 * Custom template tags for this theme.
 */
require SILVERBIRD_FUNCTIONS_DIRECTORY . '/template-tags.php';
/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require SILVERBIRD_FUNCTIONS_DIRECTORY . '/template-functions.php';

/**
 * Include custom widgets.
 */
include SILVERBIRD_FUNCTIONS_DIRECTORY . '/widgets/silverbird-recent-posts.php';
include SILVERBIRD_FUNCTIONS_DIRECTORY . '/widgets/silverbird-category-posts.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require SILVERBIRD_FUNCTIONS_DIRECTORY . '/jetpack.php';
}
