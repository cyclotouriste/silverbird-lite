=== Silverbird Lite ===

Requires at least: 4.4
Tested up to: 6.5
Stable tag: 1.1.2
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

Silverbird Lite is simply Elegant SEO friendly WordPress Theme suitable for fashion blogging, photography, food and travel with tons of options to choose from. Silverbird Lite WordPress theme is Mobile Responsive, light weight and comes fully intergrated with Social Media icons to enable your readers share your posts. The enormous features that are packed into this theme allows you to design and customize it to your satisfaction. In the homepage you can display a single large post slider or a post slider with two featured posts beside it, giving you the opportunity to highlight more posts to your readers. All the customizations can be done in the WordPress Theme customizer section. Some features in this version of Silverbird Lite includes different Header Logo positions, Header Ads widget, Posts Formats for Gallery, Video, and Audio. Post Carousel, Related Posts, Random Slide-in post appears when user scrolls to the bottom of the page, Footer Widgets, About Me Widget, Sticky Sidebars, Recent Post with featured image. Different post layouts also allows you to select if you want to display your featured image in full width.


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

= Updates =

It is hosted on Github. For updates, it might be a good idea to also use this plugin for updates from Github:
[Github Updater plugin](https://github.com/afragen/github-updater).


== Frequently Asked Questions ==

= Does this theme support any plugins? =

Silverbird Lite includes support for Infinite Scroll in Jetpack.

= I want to keep on using Silverbird =

In case you decide to keep on using Silverbird or Silverbird Pro version 1.0.4, you will probably be able to on WordPress 5.6 and PHP 8. You will need to copy some files from Silverbird Lite, specifically '/assets/js/custom.js' and '/assets/js/bootstrap(.min).js'.


== Changelog ==

= 1.1.2 =
* 2021-10-30
* Do not force threading of comments.

= 1.1.1 =
* 2021-03-17
* Fix some deprecated jQuery calls with WP 5.6 and jQuery 3.5.

= 1.1.0 =
* 2020-12-10
* Rename to Silverbird Lite.
* Tested on WP 5.6 and PHP 8.
* Fix preloader and disable it by default.
* Update Bootstrap.js from 3.3.6 to 3.4.1.
* Remove old modernizr 2.6.2.
* Remove Support tab in Customizer.
* Remove Google Fonts, there are plugins for that.
* Remove Google Plus from social media links.
* Remove Swipebox, you might prefer your own image modal.

= 1.0.4 =
* 2018-05-13
* Fixed: Sticky sidebar, default setting in customization wrongly set to true
* Fixed: Homepage slider, default setting in customization wrongly set to true
* Added: Homepage Promo Box from Pro version made available in this free version
* Added: New Category Posts Widget. Select widget to display as list or classic
* Added: Ability to select Site Title Font Family in Theme Customizer
* Added: Woocommerce Compatible - Just install Woocommerce plugin and you are ready to go

= 1.0.3 =
* 2018-04-21
* ADDED: More features
* ADDED: Custom Recent Post Widget
* Fixed: Minor issues

= 1.0.2 =
^ 2018-04-19
* Fixed: Undefined variable: $length in template-functions.php
* Fixed: Mino issues
* ADDED: Language POT for translation

= 1.0.1 =
* 2018-03-20
* Fixed: Required issues for WordPress Theme reviewing. Minor bugs

= 1.0.0 =
* 2018-01-30
* Initial release



--------------------
Licenses
--------------------
Silverbird WordPress Theme, Copyright 2018        WP3Layouts.com
Silverbird Lite WordPress Theme, Copyright 2020 - 2020 Marcel Pol
Silverbird Lite Theme is distributed under the terms of the GNU GPL

Unless otherwise specified, all the theme files, scripts and images
are licensed under GNU General Public License v2 or later

See headers of files for further details.

== LICENCE / CREDITS ==

	* Theme Screenshot images
	  CC0-licensed (GPL-compatible) images from http://pixabay.com/
	  pixabay Image ID: 2003647 		https://pixabay.com/en/beautiful-girl-in-the-park-2003647/
	  pixabay Image ID: 2907463 		https://pixabay.com/en/girl-nature-waiting-blue-dress-2907463/
	  pixabay Image ID: 410328			https://pixabay.com/en/waiting-appointment-schedule-time-410328/
	  pixabay Image ID: 1209416			https://pixabay.com/en/necktie-tie-fashion-beard-male-1209416/
	  pixabay Image ID: 3299379         https://pixabay.com/en/woman-portrait-fashion-lovely-girl-3299379/
	  pixabay Image ID: 3338647         https://pixabay.com/en/nature-lovely-woman-summer-3338647/
	  pixabay Image ID: 3281388         https://pixabay.com/en/mother-and-daughter-adult-women-two-3281388/
	  pixabay Image ID: 1478810 		https://pixabay.com/en/fashion-glasses-go-pro-female-1478810/
	  pixabay Image ID: 2808475 		https://pixabay.com/en/girl-summer-portrait-house-person-2808475/
	  pixabay Image ID: 3328270 		https://pixabay.com/en/fashion-lovely-portrait-woman-3328270/
	  pixabay Image ID: 3350094			https://pixabay.com/en/woman-portrait-fashion-young-adult-3350094/

	* Bootstrap v3.4.1 (http://getbootstrap.com)
	  Copyright 2011-2019 Twitter, Inc.
	  Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

	* Bootstrap Script v3.4.1 (http://getbootstrap.com)
	  Copyright 2011-2019 Twitter, Inc.
	  Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

	* Icons: Font Awesome 4.6.3 by @davegandy - http://fontawesome.io - @fontawesome
	  License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
	  Licensed under:  SIL OFL 1.1 (http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)
	  Copyright: Dave Gandy - http://fontawesome.io/

	  CSS: Font Awesome
	  Font Awesome 4.4.0 by @davegandy - http://fontawesome.io - @fontawesome
      License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)

	* Animate.css -http://daneden.me/animate
	  Licensed under the MIT license - http://opensource.org/licenses/MIT
	  Copyright (c) 2014 Daniel Eden

	* SlickNav Responsive Mobile Menu v1.0.10 CSS
	  Licensed under the MIT license - http://opensource.org/licenses/MIT
	  (c) 2016 Josh Cope

	* jQuery Easing v1.4.1 - George McGinley Smith - http://gsgd.co.uk/sandbox/jquery/easing/
	  Open source under the BSD License
	  Copyright © 2008 George McGinley Smith

	* Core Owl Carousel CSS File v1.3.3
	  Licensed under the MIT license - http://opensource.org/licenses/MIT

	* Owl Carousel CSS3 Transitions v1.3.2
	  Licensed under the MIT license - http://opensource.org/licenses/MIT

	* Owl Carousel Owl Demo Theme v1.3.3
	  Licensed under the MIT license - http://opensource.org/licenses/MIT

	* FitVids 1.1
	  Copyright 2013, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
	  Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
	  Released under the WTFPL license - http://sam.zoy.org/wtfpl/

	* jQuery-stickit v0.2.14
	  https://github.com/emn178/jquery-stickit
	  Copyright 2014-2017, emn178@gmail.com
	  Licensed under the MIT license - http://opensource.org/licenses/MIT

	* SlickNav Responsive Mobile Menu v1.0.10 JS
	  Licensed under the MIT license - http://opensource.org/licenses/MIT
	  (c) 2016 Josh Cope

	* Silverbird Lite is based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc.,
	  [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

	* Scripts
	  Scripts used in this Theme are licensed under the GNU General Public License
	  (http://www.gnu.org/licenses/gpl-2.0.html)
