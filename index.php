<?php
/**
 * The main template file
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

get_header(); ?>

<?php  silverbird_homepage_items(); ?>

	<div class="main-content">
        <div class="container">
            <div class="row">
            	<div class="col-md-8 col-sm-8">
				<?php
				if ( have_posts() ) :

					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/*
						 * Get the various template parts
						 */
						get_template_part( 'template-parts/content', 'blockview' );


					endwhile;

					the_posts_pagination(
					    array(
		                    'mid_size' => 3,
		                    'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i> ',
		                    'next_text' => ' <i class="fa fa-arrow-right" aria-hidden="true"></i> ',
		                )
		            );

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>
			</div>

			<?php
				get_sidebar();
			?>

			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .main-content -->

<?php
get_footer();
