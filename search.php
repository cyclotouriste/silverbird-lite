<?php
/**
 * The template for displaying search results pages
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

get_header(); ?>

	<div class="main-content">
        <div class="container">
            <div class="row">
            	<div class="col-md-8 col-sm-8">

					<?php
					if ( have_posts() ) : ?>

						<header class="page-header">
							<h1 class="page-title"><?php
								/* translators: %s: search query. */
								printf( esc_html__( 'Search Results for: %s', 'silverbird' ), '<span>' . get_search_query() . '</span>' );
							?></h1>
						</header><!-- .page-header -->

						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/**
							 * Get the various template parts
							 */
							get_template_part( 'template-parts/content', 'search' );


						endwhile;

						the_posts_pagination(
						    array(
			                    'mid_size' => 3,
			                    'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i> ',
			                    'next_text' => ' <i class="fa fa-arrow-right" aria-hidden="true"></i> ',
			                )
			            );

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif; ?>

				</div><!-- .col-md-8-->
				<?php
					get_sidebar();
				?>

			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .main-content -->

<?php
get_footer();
