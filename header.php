<?php
/**
 * Theme header
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="wrapper">

   <?php silverbird_preloader(); ?>

    <!--header section start-->
    <header id="header" class="site-header">

        <?php silverbird_header(); ?>

        <div id="nav-menu">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="main-menu text-uppercase">
                            <?php
								wp_nav_menu( array(
									'theme_location' => 'main-menu',
									'menu_id'        => 'menu',
                                    'link_after' => ' <i class="fa fa-angle-down"></i>',
								) );
							?>

                            <?php silverbird_menu_search_btn(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--header section end-->
