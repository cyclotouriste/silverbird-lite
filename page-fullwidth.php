<?php
/**
 * Template Name: Full Width
 *
 * The template for displaying Full Width pages
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

get_header();
?>

	<div class="main-content">
        <div class="container">
            <div class="row">
            	<div class="col-md-12">

					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>

				</div><!-- .col-md-12 -->

			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .main-content -->

<?php
get_footer();
