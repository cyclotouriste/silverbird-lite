<?php
/**
 * Custom template tags for this theme
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

if ( ! function_exists( 'silverbird_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function silverbird_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'silverbird' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'silverbird' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;


function silverbird_category_list() {
	// Hide category for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ' / ', 'silverbird' ) );
		if ( $categories_list ) {
			/* translators: 1: list of categories. */
			printf( '<span class="cat">' . esc_html__( ' %1$s', 'silverbird' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}
	}
}

function silverbird_tag_list() {
	if(get_theme_mod('silverbird_single_tags', true ) == true ){
		// Hide category for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ' ', 'list item separator', 'silverbird' ) );
				if ( $tags_list ) {
					/* translators: 1: list of tags. */
					printf( '<span class="tags-links"><i class="fa fa-tags" aria-hidden="true"></i> ' . esc_html__( ' %1$s', 'silverbird' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}
	}
}

if ( ! function_exists( 'silverbird_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function silverbird_entry_footer() {

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'silverbird' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'silverbird_post_thumbnail' ) ) :
/**
 * Displays an optional post thumbnail.
 *
 * Wraps the post thumbnail in an anchor element on index views, or a div
 * element when on single views.
 */
function silverbird_post_thumbnail() {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
		if(has_post_thumbnail()){
	?>
		<div class="post-thumbnail">
			<?php the_post_thumbnail(); ?>
		</div><!-- .post-thumbnail -->
	<?php } ?>
	<?php else : ?>
		<?php if(has_post_thumbnail()){ ?>
		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
			<?php
				the_post_thumbnail( 'silverbird-thumbnail', array(
					'alt' => the_title_attribute( array(
						'echo' => false,
					) ),
				) );
			?>
		</a>
		<?php } ?>
	<?php endif; // End is_singular().
}
endif;


function silverbird_bottom(){
	?>

	<!--footer start-->
    <footer id="footer">
        <div class="footer-widget-section <?php  if ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || get_theme_mod('silverbird_social_facebook','') || get_theme_mod('silverbird_social_twitter','') || get_theme_mod('silverbird_social_gplus','') || get_theme_mod('silverbird_social_youtube','') || get_theme_mod('silverbird_social_instagram','') || get_theme_mod('silverbird_social_pinterest','') || get_theme_mod('silverbird_social_rssfeed','') ) { ?> ft-bg <?php } ?>">
            <div class="container">
                <div class="row">
                  <?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
                    <div class="col-md-4">
                        <aside class="footer-widget">
                            <?php dynamic_sidebar('footer-1'); ?>
                        </aside>
                    </div>
                    <?php } if ( is_active_sidebar( 'footer-2' ) ) { ?>
                    <div class="col-md-4">
                        <aside class="footer-widget">
                            <?php dynamic_sidebar('footer-2'); ?>
                        </aside>
                    </div>
                    <?php } if ( is_active_sidebar( 'footer-3' ) ) { ?>
                    <div class="col-md-4">
                        <aside class="footer-widget">
                            <?php dynamic_sidebar('footer-3'); ?>
                        </aside>
                    </div>
                    <?php } ?>

                    <?php
                        /* calling the social follow buttons*/
                        do_action('silverbird_social_follow');
                    ?>
                </div><!--row end-->
            </div>
        </div>

        <div class="footer-copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                    	<?php if( get_theme_mod('silverbird_footer_copyright','') != '' ){ printf( esc_attr__( '%s ', 'silverbird' ), get_theme_mod('silverbird_footer_copyright') ); } else { ?>
                        <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'silverbird' ) ); ?>">
	                        <?php
								/* translators: %s: CMS name, i.e. WordPress. */
								printf( esc_html__( 'Proudly powered by %s', 'silverbird' ), 'WordPress' );
							?>
						</a>
						<?php } ?>
                    </div>

                    <div class="col-md-6 text-right">
						<?php
							/* translators: 1: Theme name, 2: Theme author. */
							// FIXME: what to do in this column, nothing?
							//printf( esc_html__( 'Theme: %1$s by %2$s.', 'silverbird' ), '<a href="http://www.wp3layouts.com/silverbird-elegant-wordpress-blog-theme/" target="_blank">SilverBird</a>', 'WP3Layouts' );
						?>

                        <div class="gototop js-top">
							<a href="#" class="js-gotop"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--footer end-->

    <?php get_template_part( 'sections/custom', 'search' ); ?>

</div><!--.wrapper end-->

<?php wp_footer(); ?>

</body>
</html>

	<?php
}
