<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

if ( ! function_exists( 'silverbird_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function silverbird_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Silverbird, use a find and replace
		 * to change 'silverbird' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'silverbird', SILVERBIRD_DIRECTORY . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Post formats
		add_theme_support( 'post-formats', array( 'standard', 'gallery', 'video', 'audio' ) );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size('silverbird-large', 1920, 850, array('center', 'center') );
		add_image_size('silverbird-home-slider', 1140, 550, true );
		add_image_size('silverbird-thumbnail', 750, 480, array('center', 'center') );
		add_image_size('silverbird-posts-nav', 360, 140, array('center', 'top') );
		add_image_size('silverbird-related', 217, 160, array('center', 'top') );
		add_image_size('silverbird-tiny', 120, 90, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'main-menu' => esc_html__( 'Main Menu', 'silverbird' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );



		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'silverbird_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 100,
			'width'       => 320,
			'flex-width'  => true,
			'flex-height' => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );

		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}
endif;
add_action( 'after_setup_theme', 'silverbird_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function silverbird_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'silverbird_content_width', 640 );
}
add_action( 'after_setup_theme', 'silverbird_content_width', 0 );

/**
 * Register widget area.
 *
 */
function silverbird_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'silverbird' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'silverbird' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	if ( get_theme_mod('silverbird_logomenu_layout', 'logocenter' ) == 'logoads' ):
	register_sidebar( array(
		'name'          => esc_html__( 'Header Ads', 'silverbird' ),
		'id'            => 'sb-header-ads',
		'description'   => esc_html__( 'Add Banner Ads Here. Recommended size: 728x90', 'silverbird' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	endif;
	if ( function_exists( 'is_woocommerce' ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Woocommerce Sidebar', 'silverbird' ),
			'id'            => 'woocommerce-sidebar',
			'description'   => esc_html__( 'Sidebar for Woocommerce.', 'silverbird' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'silverbird' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'silverbird' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'silverbird' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'silverbird' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'silverbird' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'silverbird' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'silverbird_widgets_init' );

function silverbird_homepage_items(){
	if ( silverbird_live_preview() ) {
		get_template_part( 'core/preview/slider' );
		get_template_part( 'core/preview/promo' );
	} else {
		get_template_part( 'sections/slider' );
		get_template_part( 'sections/promo','box' );
	}
}


function silverbird_preloader(){
	if ( get_theme_mod('silverbird_preloader', false ) == true ) {
	?>
	<!-- pre-loader-->
	<div class="preloader">
		<div class="spinner">
			<div class="double-bounce1"></div>
			<div class="double-bounce2"></div>
		</div>
	</div>

<?php
	}
}

function silverbird_menu_search_btn(){
	if ( get_theme_mod( 'silverbird_menu_search_btn', true ) == true) {
		?>
		<div class="search-container">
			<span id="menu-search-btn"><i class="fa fa-search"></i></span>
		</div>
		<?php
	}
}

//Check if theme is live preview
function silverbird_live_preview() {
	$theme    	  = wp_get_theme();
	$theme_name   = $theme->get( 'TextDomain' );
	$active_theme = silverbird_get_raw_option( 'template' );
	return apply_filters( 'silverbird_live_preview', ( $active_theme != strtolower( $theme_name ) && ! is_child_theme() ) );
}

//Get Raw Options
function silverbird_get_raw_option( $opt_name ) {
	$alloptions = wp_cache_get( 'alloptions', 'options' );
	$alloptions = maybe_unserialize( $alloptions );
	return isset( $alloptions[ $opt_name ] ) ? maybe_unserialize( $alloptions[ $opt_name ] ) : false;
}



/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function silverbird_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'silverbird_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function silverbird_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'silverbird_pingback_header' );


// Excerpt Length
function silverbird_custom_excerpt_length( $length ) {
	if ( is_admin() )
	{
		return $length;
	}

	$length =  absint( get_theme_mod('silverbird_excerpt_length', 30 ) );

	return $length;
}
add_filter( 'excerpt_length', 'silverbird_custom_excerpt_length', 999 );

// Excerpt More
function silverbird_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	if( get_theme_mod('silverbird_readmore_display', true ) == true ){
		$readmore = get_theme_mod('silverbird_readmore_txt', 'Continue Reading' );

		$link = sprintf( '<div class="btn-continue-reading text-center text-uppercase"><a href="%1$s" class="more-link">%2$s</a></div>',
			esc_url( get_permalink( get_the_ID() ) ),
			/* translators: %s: Name of current post */
			sprintf( esc_attr(  ' %s ', 'silverbird' ), $readmore )
		);

		return ' &hellip; ' . $link;

	} else {

		return ' &hellip; ';

	}

}
add_filter( 'excerpt_more', 'silverbird_excerpt_more' );


function silverbird_header(){ ?>
	<?php if ( get_theme_mod('silverbird_logomenu_layout', 'logocenter' ) == 'logocenter' ) :?>
	 	<div class="main-logo site-branding text-center">
		   <?php if( get_theme_mod( 'custom_logo' ) ){ ?>
				<div class="img-logo">
					<?php the_custom_logo(); ?>
				</div>
			<?php } else { ?>
				<h1 class="site-title text-center">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' );?></a>
				</h1>
				<p class="site-description text-center">
					<?php bloginfo( 'description' );?>
				</p>
			<?php } ?>
		</div>
	<?php else: ?>
		<div class="main-logo site-branding container">
			<div class="logo-section">
			   <?php if( get_theme_mod( 'custom_logo' ) ){ ?>
					<div class="img-logo">
						<?php the_custom_logo(); ?>
					</div>
				<?php } else { ?>
					<h1 class="site-title text-center">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' );?></a>
					</h1>
					<p class="site-description text-center">
						<?php bloginfo( 'description' );?>
					</p>
				<?php } ?>
			</div>

			<?php if ( get_theme_mod('silverbird_logomenu_layout', 'logocenter' ) == 'logoads' ): ?>

				<div class="ad-section">
					<?php dynamic_sidebar('sb-header-ads'); ?>
				</div>

			<?php else:?>

			<div class="hd-social-section">

				<?php do_action( 'silverbird_social_follow' ); ?>

			</div>
			<?php endif;?>

		</div>
	<?php endif; ?>
<?php }


function silverbird_social_cb(){
	$facebook = get_theme_mod('silverbird_social_facebook','');
	$twitter = get_theme_mod('silverbird_social_twitter','');
	$youtube = get_theme_mod('silverbird_social_youtube','');
	$instagram = get_theme_mod('silverbird_social_instagram','');
	$pinterest = get_theme_mod('silverbird_social_pinterest','');
	$rssfeed = get_theme_mod('silverbird_social_rssfeed','');
	?>
	<?php if ( $facebook || $twitter || $youtube || $instagram || $pinterest || $rssfeed ) : ?>
		<ul class="social-follow">

			<li><span id="search-btn"><i class="fa fa-search"></i></span></li>

			<?php if (!empty($facebook)) { ?>
				<li><a href="<?php echo esc_url($facebook)?>" class="facebook" title="Facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<?php
			}
			if (!empty($twitter)) {
			?>
				<li><a href="<?php echo esc_url($twitter)?>" class="twitter" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<?php
			}
			if (!empty($youtube)) {
			?>
				<li><a href="<?php echo esc_url($youtube)?>" class="youtube" title="YouTube" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
			<?php
			}
			if (!empty($instagram)) {
			?>
				<li><a href="<?php echo esc_url($instagram)?>" class="instagram" title="Instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<?php
			}
			if (!empty($pinterest)) {
			?>
				<li><a href="<?php echo esc_url($pinterest)?>" class="pinterest" title="Pinterest" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
			<?php
			}
			if (!empty($rssfeed)) {
			?>
				<li><a href="<?php echo esc_url($rssfeed)?>" class="rss" title="RSS Feed" target="_blank"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
			<?php
			}
			?>
		</ul>
	<?php

	endif;
}
add_action( 'silverbird_social_follow', 'silverbird_social_cb' );

function silverbird_author_box(){
	if(get_theme_mod('silverbird_single_author_box', true ) == true ){
	?>
	<div class="autho-box"><!--autho box-->
			<span class="pull-left"><?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '100' );  } ?></span>

			<div class="author-name"><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>" rel="nofollow" class="fn"><?php the_author(); ?></a></div>

			<?php if( get_the_author_meta( 'description' ) ) { ?>
				<p><?php the_author_meta('description') ?></p>
			<?php }?>
		</div><!--autho box end-->
	<?php
	}
}

function silverbird_posts_navigation(){
	$prevThumb = get_the_post_thumbnail_url( get_previous_post(), 'silverbird-posts-nav' );
	$nextThumb = get_the_post_thumbnail_url( get_next_post(), 'silverbird-posts-nav' );

	$previous_link_url = get_permalink( get_previous_post() );
	$next_link_url = get_permalink( get_next_post() );
	?>
	<div class="row">
		<div class="col-md-6">
			<div class="single-blog-box">
				<a href="<?php echo esc_url($previous_link_url);  ?>">
					<img src="<?php if(!empty($prevThumb)){ echo esc_url($prevThumb); } else { echo esc_url(SILVERBIRD_DIRECTORY_URI) . '/assets/images/prev.jpg'; } ?>" alt="<?php the_title_attribute();?>">

					<div class="overlay">
						<div class="promo-text">
							<p><i class="fa fa-angle-left"></i> <?php esc_html_e( 'Previous Post', 'silverbird' ) ?></p>
							<h5 class="text-center"><?php printf( esc_html__( '%s ', 'silverbird' ), get_the_title( get_previous_post() ) );?></h5>
						</div>
					</div>
				</a>
			</div><!--single-blog-box end-->
		</div><!--col-md-6 end-->
		<div class="col-md-6">
			<div class="single-blog-box">
				<a href="<?php echo esc_url($next_link_url);  ?>">
					<img src="<?php if(!empty($nextThumb)){ echo esc_url($nextThumb); } else { echo esc_url(SILVERBIRD_DIRECTORY_URI) . '/assets/images/next.jpg'; } ?>" alt="<?php the_title_attribute();?>">

					<div class="overlay">
						<div class="promo-text">
							<p><?php esc_html_e( 'Next Post', 'silverbird' ); ?> <i class="fa fa-angle-right"></i></p>
								<h5 class="text-center"><?php printf( esc_html__( '%s ', 'silverbird' ), get_the_title( get_next_post() ) ); ?></h5>

						</div>
					</div>
				</a>
			</div><!--single-blog-box end-->
		</div><!--col-md-6 end-->
	</div><!--row end-->
	<?php
}

function silverbird_related_posts() {

		wp_reset_postdata();

		$orderby = get_theme_mod('silverbird_single_related_orderby', 'date' );
		$num = get_theme_mod('silverbird_single_related_numbers', 4 );
		global $post;

		// Define shared post arguments
		$args = array(
			'no_found_rows'				=> true,
			'ignore_sticky_posts'		=> 1,
			'orderby'					=> $orderby,
			'post__not_in'				=> array($post->ID),
			'posts_per_page'			=> absint($num),
		);
		// Related by categories
		if ( get_theme_mod('silverbird_single_related_type', 'categories') == 'categories' ) {
			$cats = get_post_meta($post->ID, 'related-cat', true);
			if ( !$cats ) {
				$cats = wp_get_post_categories($post->ID, array('fields'=>'ids'));
				$args['category__in'] = $cats;
			} else {
				$args['cat'] = $cats;
			}
		}
		// Related by tags
		if ( get_theme_mod('silverbird_single_related_type', 'categories') == 'tags' ) {
			$tags = get_post_meta($post->ID, 'related-tag', true);
			if ( !$tags ) {
				$tags = wp_get_post_tags($post->ID, array('fields'=>'ids'));
				$args['tag__in'] = $tags;
			} else {
				$args['tag_slug__in'] = explode(',', $tags);
			}
			if ( !$tags ) { $break = true; }
		}

		$query = !isset($break)?new WP_Query($args):new WP_Query;
		return $query;
}
