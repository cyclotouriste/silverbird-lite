<?php
/**
 * Preview Template
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */
?>
 <div class="widget">
    <h2 class="widget-title"><?php esc_html_e('List View Widget','silverbird');?> </h2>
    <div class="clearfix sb-cat-posts-list">

                    <div class="clearfix post-items">
                        <figure class="entry-image">
                                <a href="#">
                                    <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/woman-3350094_640.jpg" class="wp-post-image" alt="<?php the_title_attribute(); ?>" />
                                </a>
                        </figure>
                        <div class="entry-post-details">
                            <h4 class="entry-post-title">
                                <a href="#">
                                    <?php esc_html_e('Vogue festival: Dolce and Gabbana fashion week','silverbird');?>
                                </a>
                            </h4>
                        </div>
                    </div>

                    <div class="clearfix post-items">
                        <figure class="entry-image">
                                <a href="#">
                                    <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/girl-2907463_640.jpg" class="wp-post-image" alt="<?php the_title_attribute(); ?>" />
                                </a>
                        </figure>
                        <div class="entry-post-details">
                            <h4 class="entry-post-title">
                                <a href="#">
                                    <?php esc_html_e('At that moment she scented a panther','silverbird');?>
                                </a>
                            </h4>
                        </div>
                    </div>

                    <div class="clearfix post-items">
                        <figure class="entry-image">
                                <a href="#">
                                    <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/fashion-3328270_640.jpg" class="wp-post-image" alt="<?php the_title_attribute(); ?>" />
                                </a>
                        </figure>
                        <div class="entry-post-details">
                            <h4 class="entry-post-title">
                                <a href="#">
                                    <?php esc_html_e('The grapes seemed ready to burst with juice.','silverbird');?>
                                </a>
                            </h4>
                        </div>
                    </div>

    </div>
</div>

 <div class="widget">
    <h2 class="widget-title"><?php esc_html_e('Classic View Widget','silverbird');?> </h2>
    <div class="clearfix sb-cat-posts-classic">

                    <div class="clearfix post-items">
                        <figure class="entry-image">
                                <a href="#">
                                    <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/beautiful-girl-2003647_640.jpg" class="wp-post-image" alt="<?php the_title_attribute(); ?>" />
                                </a>
                        </figure>
                        <div class="entry-post-details">
                            <h4 class="entry-post-title">
                                <a href="#">
                                    <?php esc_html_e('Do not be too hard to suit or you may have to be content','silverbird');?>
                                </a>
                            </h4>
                        </div>
                    </div>

                    <div class="clearfix post-items">
                        <figure class="entry-image">
                                <a href="#">
                                    <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/girl-2907463_640.jpg" class="wp-post-image" alt="<?php the_title_attribute(); ?>" />
                                </a>
                        </figure>
                        <div class="entry-post-details">
                            <h4 class="entry-post-title">
                                <a href="#">
                                    <?php esc_html_e('At that moment she scented a panther','silverbird');?>
                                </a>
                            </h4>
                        </div>
                    </div>


    </div>
</div>
