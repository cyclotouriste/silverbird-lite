<?php
/**
 * Preview Template
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */
?>

<!-- Live Preview slider section start-->
    <div class="slider-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="home-carousel">

                        <div class="home-slider">
                            <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/nature-3338647_1920-1140x550.jpg" alt="<?php the_title_attribute();?>">

                            <div class="slider-overlay text-center">
                                <h2 class="entry-title text-center"><?php esc_html_e('Vogue festival: Dolce and Gabbana fashion week','silverbird');?></h2>
                                <a href="#" class="btn read-more text-uppercase">
                                    <?php esc_html_e('Read More','silverbird');?>
                                </a>
                            </div>
                        </div>

                        <div class="home-slider">
                            <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/woman-3299379-1140x550.jpg" alt="<?php the_title_attribute();?>">

                            <div class="slider-overlay text-center">
                                <h2 class="entry-title text-center"><?php esc_html_e('Why cindy change a thing is beautiful Lifestyle','silverbird');?></h2>
                                <a href="#" class="btn read-more text-uppercase">
                                    <?php esc_html_e('Read More','silverbird');?>
                                </a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
