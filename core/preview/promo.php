<?php
/**
 * Preview Template
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */
?>

    <!--promo box start-->
<div class="promo-box text-uppercase text-center">
    <div class="container">
        <div class="row">

            <div class="col-md-4">
                <div class="single-promo-box">
                    <a href="#">
                        <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/fashion-3328270_640.jpg" alt="<?php the_title_attribute();?>">

                        <div class="overlay">
                            <h3 class="promo-title">
                                <span><?php esc_html_e('About Me','silverbird');?></span>
                            </h3>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4">
                <div class="single-promo-box">
                    <a href="#">
                        <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/fashion-1478810_640.jpg" alt="<?php the_title_attribute();?>">

                        <div class="overlay">
                            <h3 class="promo-title">
                                <span><?php esc_html_e('My Portfolio','silverbird');?></span>
                            </h3>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4">
                <div class="single-promo-box">
                    <a href="#">
                        <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/core/preview/images/girl-2808475_640.jpg" alt="<?php the_title_attribute();?>">

                        <div class="overlay">
                            <h3 class="promo-title">
                                <span><?php esc_html_e('Contact Me','silverbird');?></span>
                            </h3>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
<!--promo box end-->
