<?php
/**
 * Plugin Name: Category Posts Widget
 * Description: Widget that show Recent Posts from specific category
 * Version: 1.1
 * Author: MPolleke
 * Author URI: https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

class Silverbird_Category_Posts extends WP_Widget {


	/**
	 * Register widget
	**/
	function __construct() {

		parent::__construct(
	 		'silverbird_category_posts', // Base ID
			__( 'SilverBird: Posts By Category', 'silverbird' ), // Name
			array( 'description' => __( 'Show posts from selected category', 'silverbird' ), ) // Args
		);

	}


	/**
	 * Front-end display of widget
	**/
	function widget( $args, $instance ) {

		extract( $args );

		$title = apply_filters('widget_title', isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : 'Category Title' );
		$items_num = isset( $instance['items_num'] ) ? esc_attr( $instance['items_num'] ) : '3';
		$cat_name = isset( $instance['cat_name'] ) ? esc_attr( $instance['cat_name'] ) : '';
		$widget_type = isset( $instance['widget_type'] ) ? $instance['widget_type'] : 'sb-cat-posts-list';

		/**
		 * Latest Posts
		**/
		global $post;
		$silverbird_cat_posts = new WP_Query(
			array(
				'post_type' => 'post',
				'cat' => $cat_name,
				'posts_per_page' => $items_num,
				'post__not_in' => array( $post->ID ),
				'ignore_sticky_posts' => 1,
                'no_found_rows' => true
			)
		);

		if ( $silverbird_cat_posts->have_posts() ):

			echo $before_widget;
            if ( $title ) echo $before_title . $title . $after_title;
            ?>

            <div class="clearfix <?php echo $widget_type; ?>">
                <?php while ( $silverbird_cat_posts->have_posts() ) : $silverbird_cat_posts->the_post(); ?>

                    <div class="clearfix post-items">
                        <?php if ( has_post_thumbnail() ) { ?>
                            <figure class="entry-image">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail( 'silverbird-thumbnail' ); ?>
                                </a>
                            </figure>
                        <?php } elseif( silverbird_first_post_image() ) { ?>
                            <figure class="entry-image">
                                <a href="<?php the_permalink(); ?>">
                                    <img src="<?php echo esc_url( silverbird_first_post_image() ); ?>" class="wp-post-image" alt="<?php the_title(); ?>" />
                                </a>
                            </figure>
                        <?php } ?>

                        <div class="entry-post-details">
                            <h4 class="entry-post-title">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a>
                            </h4>
                        </div>
                    </div>

                <?php endwhile; ?>
            </div>

            <?php
            echo $after_widget;
			wp_reset_postdata();

		endif;

	}


	/**
	 * Sanitize widget form values as they are saved
	**/
	function update( $new_instance, $old_instance ) {

		$instance = array();

		/* Strip tags to remove HTML. For text inputs and textarea. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['items_num'] = strip_tags( $new_instance['items_num'] );
		$instance['cat_name'] = strip_tags( $new_instance['cat_name'] );
		$instance['widget_type'] = $new_instance['widget_type'];

		return $instance;

	}


	/**
	 * Back-end widget form
	**/
	function form( $instance ) {

		/* Default widget settings. */
		$defaults = array(
			'title' => 'Category Title',
			'items_num' => '3',
			'cat_name' => '',
			'widget_type' => 'sb-cat-posts-list'
		);
		$instance = wp_parse_args( (array) $instance, $defaults );

	?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'silverbird'); ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'items_num' ); ?>"><?php esc_html_e('Maximum posts to show:', 'silverbird'); ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'items_num' ); ?>" name="<?php echo $this->get_field_name( 'items_num' ); ?>" value="<?php echo $instance['items_num']; ?>" size="1" />
		</p>
        <p>
        	<label for="<?php echo $this->get_field_id( 'cat_name' ); ?>"><?php esc_html_e('Select Category:', 'silverbird'); ?></label>
        	<select id="<?php echo $this->get_field_id( 'cat_name' ); ?>" name="<?php echo $this->get_field_name( 'cat_name' ); ?>" class="widefat">
        	<?php
			$categories = get_categories();
			  foreach( $categories as $category ) {
			?>
				<option value="<?php echo $output_categories[] = $category->cat_ID; ?>" <?php if ( $instance["cat_name"] == $category->cat_ID ) echo 'selected="selected"'; ?>>
					<?php echo $output_categories[$category->cat_ID] = $category->name; ?>
                </option>
			<?php } ?>
            </select>
        </p>
        <p>
        	<label><?php esc_html_e( 'Posts Layout:', 'silverbird' ); ?></label><br />

			<input type="radio" id="<?php echo $this->get_field_id( 'sb-cat-posts-list' ); ?>" name="<?php echo $this->get_field_name( 'widget_type' ); ?>" <?php if ($instance["widget_type"] == 'sb-cat-posts-list') echo 'checked="checked"'; ?> value="sb-cat-posts-list" />
            <label for="<?php echo $this->get_field_id( 'sb-cat-posts-list' ); ?>"><?php esc_html_e( 'Display as List', 'silverbird' ); ?></label><br />

            <input type="radio" id="<?php echo $this->get_field_id( 'sb-cat-posts-classic' ); ?>" name="<?php echo $this->get_field_name( 'widget_type' ); ?>" <?php if ($instance["widget_type"] == 'sb-cat-posts-classic') echo 'checked="checked"'; ?> value="sb-cat-posts-classic" />
            <label for="<?php echo $this->get_field_id( 'sb-cat-posts-classic' ); ?>"><?php esc_html_e( 'Display as Classic', 'silverbird' ); ?></label>
        </p>
	<?php
	}

}
register_widget( 'Silverbird_Category_Posts' );
