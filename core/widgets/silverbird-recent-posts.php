<?php
/**
 * Plugin Name: Recent Posts Widget
 * Description: Widget that show Recent Posts
 * Version: 1.1
 * Author: MPolleke
 * Author URI: https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

/**
 * Register the Widget
 */
class Silverbird_Recent_Posts_Widget extends WP_Widget {

	public function __construct() {

		parent::__construct(
	 		'silverbird-recent-post-widget', // Widget ID
			__( 'SilverBird: Recent Posts', 'silverbird' ), // Name
			array( 'description' => __( 'Display recent posts with featured image', 'silverbird' ), )
		);

	}

	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts', 'silverbird' );

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		/*
		* Query the Recent Posts
		*/
		$recent_post = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => absint($number),
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) ) );

		if ($recent_post->have_posts()) :
		?>
		<?php echo $args['before_widget']; ?>
		<?php if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>
		<div class="sb-latest-posts">
		<?php while ( $recent_post->have_posts() ) : $recent_post->the_post(); ?>

			<div class="media">

				<?php if( has_post_thumbnail() ):?>
					<div class="pull-left">
						<a href="<?php the_permalink(); ?>">
							<div id="post-image">
								<img src="<?php the_post_thumbnail_url('silverbird-tiny'); ?>" class="img-responsive" alt="<?php the_title(); ?>">
							</div>
						</a>
					</div>
				<?php endif;?>

				<div class="media-body">
					<div class="title">
						<a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
					</div>
					<?php if ( $show_date ) : ?>
						<span class="entry-meta"><?php echo get_the_date(); ?></span>
					<?php endif; ?>
				</div>
				<div class="clearfix"> </div>
			</div>

		<?php endwhile; ?>
		</div>
		<?php echo $args['after_widget']; ?>
		<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;
	}

	/**
	 * Updating the instance.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		return $instance;
	}

	/**
	 * Outputs the settings
	 */
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'silverbird' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'silverbird' ); ?></label>
		<input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox"<?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?', 'silverbird' ); ?></label></p>
<?php
	}
}

register_widget( 'Silverbird_Recent_Posts_Widget' );
