<?php
/**
 * Custom CSS Output file
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

function silverbird_wp3_customize_css() {

    $sb_bg_color       = get_theme_mod('background_color','');
    $sb_default_color  = get_theme_mod('silverbird_default_color','#BC9F60');
    $sb_site_title     = get_theme_mod('header_title_color','#BC9F60');
    $sb_header_text    = get_theme_mod('header_txtcolor','#000000');
    $sb_sm_icons       = get_theme_mod('silverbird_social_media_colors','grey');
    $sb_btn_color      = get_theme_mod('silverbird_button_colors','#BC9F60');
    $sb_btn_hv_color   = get_theme_mod('silverbird_button_hover_colors','#BF9232');

    // Start CSS Style
    $css = '<style id="silverbird-custom-css">';

    if ( isset( $sb_bg_color ) ) {
        $css .= 'body {
            background: ' . $sb_bg_color . ';
        }';
    }
    if ( isset( $sb_site_title ) ) {
        $css .= '
        .site-title a,
        .main-logo h1 {
            color: ' . $sb_site_title . ';
        }';
    }
    if ( isset( $sb_header_text ) ) {
        $css .= ' .site-description {
            color: ' . $sb_header_text . ';
        }';
    }
    if ( $sb_sm_icons == 'coloured' ) {

        $css .= '
        .social-follow .facebook {color: #3b5998;}
        .social-follow .twitter {color: #55acee;}
        .social-follow .pinterest {color: #bd081c;}
        .social-follow .youtube {color: #CD201F;}
        .social-follow .instagram {color: #BC2A76;}
        .social-follow .rss {color: #FA9D39;}';

        if ( isset( $sb_default_color ) ) {
           $css .= '
           .social-follow .facebook:hover,
           .social-follow .twitter:hover,
           .social-follow .youtube:hover,
           .social-follow .instagram:hover,
           .social-follow .pinterest:hover,
           .social-follow .rss:hover{
                color:'. $sb_default_color .';
            }';
        }

    }

    if ( isset( $sb_default_color ) ) {

        $css .= '
        a,
        .thumb-latest-posts .entry-meta a:hover,
        .thumb-latest-posts .media-body h4 a:hover,
        #footer .footer-copy-right a:hover,
        .footer-widget .thumb-latest-posts .media-body h4 a:hover,
        .footer-widget .about-me-social a:hover,
        .footer-instagram-title i,
        .more-link:hover,
        .main-menu li.current-menu-item > a,
        .main-menu li.current_page_item > a,
        .main-menu li a:hover,
        .main-menu ul ul a:hover,
        .main-menu ul ul ul a:hover,
        .entry-cat .cat a,
        .entry-header h2.entry-title a:hover,
        .entry-header .entry-meta span a:hover,
        .entry-meta span a,
        .entry-meta span .comment,
        .more-link,
        .sidebar .widget a:hover,
        .footer-widget .widget a:hover,
        .tags-links a,
        .autho-box .author-name a:hover,
        .related-post-carousel .owl-theme .owl-controls .owl-buttons div:hover,
        .footer-widget .sb-latest-posts .media-body .title a:hover,
        .slide-in h4 a:hover,
        .slicknav_open > a > a,
        .slicknav_open .slicknav_arrow,
        a:hover .slicknav_arrow,
        .slicknav_nav a:hover {
                color:'. $sb_default_color .';
        }';

        $css .= '
        .btn-subscribe,
        .gallery-format-slider .owl-controls .owl-buttons div,
        .social-widget a:hover,
        .footer-widget .tagcloud a:hover,
        .tags-links a:hover {
                background-color: '.$sb_default_color.';
        }';

        $css .= '
        .main-menu li:hover > a:before,
        .pagination a:hover,
        .pagination .current,
        p.go-back-home a {
                background:'. $sb_default_color.';
        }';
        $css .='
         .social-widget a:hover,
         input[type="search"]:focus,
         input[type="text"]:focus,
         input[type="email"]:focus,
         input[type="url"]:focus,
         input[type="tel"]:focus,
         input[type="password"]:focus,
         select:focus,
         textarea:focus {
                border-color: '. $sb_default_color .';
         }';
         $css .= '
          .tags-links a,
          .tags-links a:hover {
            border: 1px solid ' . $sb_default_color .';
          }';
          $css .= '
           blockquote {
                border-left: 5px solid ' . $sb_default_color .';
          }';
    }

    if ( isset( $sb_btn_color ) ) {
         $css .= '
           input[type="button"],
           input[type="submit"],
           input[type="reset"],
           button{
                background: ' . $sb_btn_color .';
          }';
    }

    if ( isset( $sb_btn_hv_color ) ) {
         $css .= '
           input[type="button"]:hover,
           input[type="submit"]:hover,
           input[type="reset"]:hover,
           button:hover,
           input[type="button"]:active,
           input[type="submit"]:active,
           input[type="reset"]:active,
           button:active,
           .search-overlay button:hover{
                background: ' . $sb_btn_hv_color .';
          }';
    }


    // End CSS Style
    $css .= '</style>';


    // return compressed CSS
   echo str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css);

   // echo $css;


}
add_action( 'wp_head', 'silverbird_wp3_customize_css');

?>
