<?php
/**
 * Customizer Theme Options Fields
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

function silverbird_theme_customizers_options( $config ) {
    return wp_parse_args( array(
        'logo_image'   => SILVERBIRD_DIRECTORY_URI .'/core/admin/images/silverbird.png',
        'color_accent' => '#016489',
        'color_back'   => '#FFFFFF',
    ), $config );
}
add_filter( 'kirki/config', 'silverbird_theme_customizers_options' );

WP3_Customizer::add_config(
    'silverbird-wp3',
        array(
        	'capability'  => 'edit_theme_options',
        	'option_type' => 'theme_mod',
) );

/** General Settings **/
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'switch',
            'settings'    => 'silverbird_sticky_sidebar',
            'label'       => esc_html__( 'Sticky Sidebar', 'silverbird' ),
            'description' => esc_html__( 'Enable or Disable the Sticky sidebars', 'silverbird' ),
            'section'     => 'silverbird_general_section',
            'default'     => false,
            'priority'    => 10,
            'choices'     => array(
                'on'  => esc_html__( 'Enabled', 'silverbird' ),
                'off' => esc_html__( 'Disabled', 'silverbird' ),
            ),
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'switch',
            'settings'    => 'silverbird_preloader',
            'label'       => esc_html__( 'Preloader Setting', 'silverbird' ),
            'description' => esc_html__( 'Enable or Disable the Animation Preload', 'silverbird' ),
            'section'     => 'silverbird_general_section',
            'default'     => false,
            'priority'    => 10,
            'choices'     => array(
                'on'  => esc_html__( 'Enabled', 'silverbird' ),
                'off' => esc_html__( 'Disabled', 'silverbird' ),
            ),
) );

/** Header & Menu Section **/
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'radio-image',
            'settings'    => 'silverbird_logomenu_layout',
            'label'       => esc_html__( 'Header Layout', 'silverbird' ),
            'description' => esc_html__( 'Select your header layout. 1. Logo with banner ad, 2. Centered Logo, 3. Logo with Social Icons & Search bar. If you select Logo with Banner ads, remember to add your banner size 728x90/Adsense code in the Widget Section. Remember to also add your social Media links in the Social Media Links section of this customizer', 'silverbird' ),
            'section'     => 'silverbird_header_section',
            'default'     => 'logocenter',
            'priority'    => 10,
            'choices'     => array(
                'logoads'     => SILVERBIRD_DIRECTORY_URI . '/core/admin/images/logoad.png',
                'logocenter'  => SILVERBIRD_DIRECTORY_URI . '/core/admin/images/logocenter.png',
                'logosocial'  => SILVERBIRD_DIRECTORY_URI . '/core/admin/images/logosocial.png',
            ),
) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'switch',
            'settings'    => 'silverbird_menu_search_btn',
            'label'       => esc_html__( 'Search Button', 'silverbird' ),
            'description' => esc_html__( 'Enable or Disable the search button in the main Menu', 'silverbird' ),
            'section'     => 'silverbird_header_section',
            'default'     => true,
            'priority'    => 10,
            'choices'     => array(
                'on'  => esc_html__( 'Enabled', 'silverbird' ),
                'off' => esc_html__( 'Disabled', 'silverbird' ),
            ),
) );

/** Slider and Featured Post **/
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'switch',
            'settings'    => 'silverbird_slides_section',
            'label'       => esc_html__( 'Frontpage Slider Section', 'silverbird' ),
            'description' => esc_html__( 'Enable or Disable the Frontpage Slider section. To add posts to the slider, remember to tick the checkbox under Post Options in the Post Edit', 'silverbird' ),
            'section'     => 'silverbird_slider_section',
            'default'     => false,
            'priority'    => 10,
            'choices'     => array(
                'on'  => esc_html__( 'Enabled', 'silverbird' ),
                'off' => esc_html__( 'Disabled', 'silverbird' ),
            ),
) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'radio-buttonset',
            'settings'    => 'silverbird_slides_width',
            'label'       => __( 'Width of Slider', 'silverbird' ),
            'description' => esc_html__( 'Select the appearance width of the slider', 'silverbird' ),
            'section'     => 'silverbird_slider_section',
            'default'     => 'centered',
            'priority'    => 10,
            'choices'     => array(
                'fullwidth' => esc_html__( 'Fullwidth', 'silverbird' ),
                'centered'  => esc_html__( 'Centered', 'silverbird' ),
            ),
) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'number',
            'settings'    => 'silverbird_slider_num',
            'label'       => esc_html__( 'Number of Slides', 'silverbird' ),
            'description' => esc_html__( 'Enter the number of slides to display. Max 10', 'silverbird' ),
            'section'     => 'silverbird_slider_section',
            'default'     => 4,
            'priority'    => 10,
            'choices'     => array(
                'min'  => 1,
                'max'  => 10,
                'step' => 1,
            ),
) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'radio-buttonset',
            'settings'    => 'silverbird_slides_orderby',
            'label'       => __( 'Slides Order By', 'silverbird' ),
            'description' => esc_html__( 'Select how you want post to display, either by Date or Randomly', 'silverbird' ),
            'section'     => 'silverbird_slider_section',
            'default'     => 'date',
            'priority'    => 10,
            'choices'     => array(
                'date' => esc_html__( 'Date', 'silverbird' ),
                'rand' => esc_html__( 'Random', 'silverbird' ),
            ),
) );

/** Promo Box **/
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'switch',
            'settings'    => 'silverbird_promo_box',
            'label'       => esc_html__( 'Promo Boxes', 'silverbird' ),
            'description' => esc_html__( 'Enable or Disable the Three Promo Boxes', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'default'     => false,
            'priority'    => 10,
            'choices'     => array(
                'on'  => esc_html__( 'Enabled', 'silverbird' ),
                'off' => esc_html__( 'Disabled', 'silverbird' ),
            ),
) );
//Box One
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_promo_one_title',
            'label'       => esc_html__( 'Box One Title', 'silverbird' ),
            'description' => esc_html__( 'Enter the title for box one', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'image',
            'settings'    => 'silverbird_promo_one_image_url',
            'label'       => esc_html__( 'Box One Image', 'silverbird' ),
            'description' => esc_html__( 'Upload background image for promo box one. Recommended px size 750x480.', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'default'     => '',
            'choices'     => array(
            'save_as' => 'id',
        ),
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_promo_one_url',
            'label'       => esc_html__( 'Box One URL', 'silverbird' ),
            'description' => esc_html__( 'Enter the full URL link for box one. eg: http://example.com/about-us ', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'priority'    => 10,
) );
//Box Two
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_promo_two_title',
            'label'       => esc_html__( 'Box Two Title', 'silverbird' ),
            'description' => esc_html__( 'Enter the title for box two', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'image',
            'settings'    => 'silverbird_promo_two_image_url',
            'label'       => esc_html__( 'Box Two Image', 'silverbird' ),
            'description' => esc_html__( 'Upload background image for promo box two. Recommended px size 750x480', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'default'     => '',
            'choices'     => array(
            'save_as' => 'id',
        ),
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_promo_two_url',
            'label'       => esc_html__( 'Box Two URL', 'silverbird' ),
            'description' => esc_html__( 'Enter the full URL link for box two. eg: http://example.com/potfolio ', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'priority'    => 10,
) );
//Box Three
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_promo_three_title',
            'label'       => esc_html__( 'Box Three Title', 'silverbird' ),
            'description' => esc_html__( 'Enter the title for box three', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'image',
            'settings'    => 'silverbird_promo_three_image_url',
            'label'       => esc_html__( 'Box Three Image', 'silverbird' ),
            'description' => esc_html__( 'Upload background image for promo box three. Recommended px size 750x480', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'default'     => '',
            'choices'     => array(
            'save_as' => 'id',
        ),
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_promo_three_url',
            'label'       => esc_html__( 'Box Three URL', 'silverbird' ),
            'description' => esc_html__( 'Enter the full URL link for box three. eg: http://example.com/contact-us ', 'silverbird' ),
            'section'     => 'silverbird_promo_section',
            'priority'    => 10,
) );

/** Posts Archive Layout **/
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'toggle',
            'settings'    => 'silverbird_posts_date',
            'label'       => esc_html__( 'Post Dates', 'silverbird' ),
            'description' => esc_html__( 'Show or hide posts date', 'silverbird' ),
            'section'     => 'silverbird_posts_settings',
            'default'     => true,
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'toggle',
            'settings'    => 'silverbird_posts_cats',
            'label'       => esc_html__( 'Post Category', 'silverbird' ),
            'description' => esc_html__( 'Show or hide posts Category', 'silverbird' ),
            'section'     => 'silverbird_posts_settings',
            'default'     => true,
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'toggle',
            'settings'    => 'silverbird_posts_comment',
            'label'       => esc_html__( 'Post Comments Info', 'silverbird' ),
            'description' => esc_html__( 'Show or hide info about posts comments', 'silverbird' ),
            'section'     => 'silverbird_posts_settings',
            'default'     => true,
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'toggle',
            'settings'    => 'silverbird_readmore_display',
            'label'       => esc_html__( 'Continue Reading', 'silverbird' ),
            'description' => esc_html__( 'Show or hide posts Continue Reading buttons', 'silverbird' ),
            'section'     => 'silverbird_posts_settings',
            'default'     => true,
            'priority'    => 10,
) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_readmore_txt',
            'label'       => esc_html__( 'Continue Reading Text', 'silverbird' ),
            'description' => esc_html__( 'You can change text to display for Read More', 'silverbird' ),
            'section'     => 'silverbird_posts_settings',
            'default'     => esc_html__('Continue Reading','silverbird'),
            'priority'    => 10,
) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'number',
            'settings'    => 'silverbird_excerpt_length',
            'label'       => esc_html__( 'Excerpt Length', 'silverbird' ),
            'description' => esc_html__( 'Enter the number of words for the excerpt length', 'silverbird' ),
            'section'     => 'silverbird_posts_settings',
            'default'     => 30,
            'priority'    => 10,
            'choices'     => array(
                'min'  => 1,
                'max'  => 100,
                'step' => 1,
            ),
) );


/** Single Posts **/
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'toggle',
            'settings'    => 'silverbird_single_cats',
            'label'       => esc_html__( 'Post Category', 'silverbird' ),
            'description' => esc_html__( 'Show or hide post categories on single view', 'silverbird' ),
            'section'     => 'silverbird_single_post_setting',
            'default'     => true,
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'toggle',
            'settings'    => 'silverbird_single_date',
            'label'       => esc_html__( 'Post Dates', 'silverbird' ),
            'description' => esc_html__( 'Show or hide post dates on single view', 'silverbird' ),
            'section'     => 'silverbird_single_post_setting',
            'default'     => true,
            'priority'    => 10,
) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'toggle',
            'settings'    => 'silverbird_single_comments_info',
            'label'       => esc_html__( 'Comments Info', 'silverbird' ),
            'description' => esc_html__( 'Show or hide post info of comments on single view', 'silverbird' ),
            'section'     => 'silverbird_single_post_setting',
            'default'     => true,
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'toggle',
            'settings'    => 'silverbird_single_tags',
            'label'       => esc_html__( 'Post Tags', 'silverbird' ),
            'description' => esc_html__( 'Show or hide post tags on single view', 'silverbird' ),
            'section'     => 'silverbird_single_post_setting',
            'default'     => true,
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'toggle',
            'settings'    => 'silverbird_single_author_box',
            'label'       => esc_html__( 'Author Box', 'silverbird' ),
            'description' => esc_html__( 'Show or hide post Author Box on single view', 'silverbird' ),
            'section'     => 'silverbird_single_post_setting',
            'default'     => true,
            'priority'    => 10,
) );

/*--- Related Posts */
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'switch',
            'settings'    => 'silverbird_single_related_posts',
            'label'       => esc_html__( 'Related Posts', 'silverbird' ),
            'description' => esc_html__( 'Enable or disable Related Posts under single post', 'silverbird' ),
            'section'     => 'silverbird_related_post_setting',
            'default'     => true,
            'priority'    => 10,
            'choices'     => array(
                'on'  => esc_html__( 'Enabled', 'silverbird' ),
                'off' => esc_html__( 'Disabled', 'silverbird' ),
            ),
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_single_related_title',
            'label'       => esc_html__( 'Related Title', 'silverbird' ),
            'description' => esc_html__( 'Enter title for the related post', 'silverbird' ),
            'default'     => 'You might also like',
            'section'     => 'silverbird_related_post_setting',
            'priority'    => 10,
) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'number',
            'settings'    => 'silverbird_single_related_numbers',
            'label'       => esc_html__( 'Number of Posts', 'silverbird' ),
            'description' => esc_html__( 'Enter the number of related posts to show', 'silverbird' ),
            'section'     => 'silverbird_related_post_setting',
            'default'     => 4,
            'priority'    => 10,
            'choices'     => array(
                'min'  => 1,
                'max'  => 50,
                'step' => 1,
            ),
) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'radio-buttonset',
            'settings'    => 'silverbird_single_related_type',
            'label'       => __( 'Posts Source', 'silverbird' ),
            'description' => esc_html__( 'Select source of related posts, either from Category or Tags', 'silverbird' ),
            'section'     => 'silverbird_related_post_setting',
            'default'     => 'categories',
            'priority'    => 10,
            'choices'     => array(
                'categories' => esc_html__( 'Category', 'silverbird' ),
                'tags' => esc_html__( 'Tag', 'silverbird' ),
            ),
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'radio-buttonset',
            'settings'    => 'silverbird_single_related_orderby',
            'label'       => __( 'Order Posts By', 'silverbird' ),
            'description' => esc_html__( 'Display related posts either by Date or Randomly', 'silverbird' ),
            'section'     => 'silverbird_related_post_setting',
            'default'     => 'date',
            'priority'    => 10,
            'choices'     => array(
                'date' => esc_html__( 'Dates', 'silverbird' ),
                'rand' => esc_html__( 'Randomly', 'silverbird' ),
            ),
) );


/*--- Social Media */
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_social_facebook',
            'label'       => esc_html__( 'Facebook', 'silverbird' ),
            'description' => esc_html__( 'Enter Your Facebook page/profile URL', 'silverbird' ),
            'section'     => 'silverbird_social_media',
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_social_twitter',
            'label'       => esc_html__( 'Twitter', 'silverbird' ),
            'description' => esc_html__( 'Enter Your Twitter page URL', 'silverbird' ),
            'section'     => 'silverbird_social_media',
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_social_youtube',
            'label'       => esc_html__( 'YourTube', 'silverbird' ),
            'description' => esc_html__( 'Enter Your YourTube channel URL', 'silverbird' ),
            'section'     => 'silverbird_social_media',
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_social_instagram',
            'label'       => esc_html__( 'Instagram', 'silverbird' ),
            'description' => esc_html__( 'Enter Your Instagram page URL', 'silverbird' ),
            'section'     => 'silverbird_social_media',
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_social_pinterest',
            'label'       => esc_html__( 'Pinterest', 'silverbird' ),
            'description' => esc_html__( 'Enter Your Pinterest page URL', 'silverbird' ),
            'section'     => 'silverbird_social_media',
            'priority'    => 10,
) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_social_rssfeed',
            'label'       => esc_html__( 'RSS Feed', 'silverbird' ),
            'description' => esc_html__( 'Enter website RSS Feed page URL', 'silverbird' ),
            'section'     => 'silverbird_social_media',
            'priority'    => 10,
) );


/** Footer **/
//Copyright
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'text',
            'settings'    => 'silverbird_footer_copyright',
            'label'       => esc_html__( 'Copyright', 'silverbird' ),
            'description' => esc_html__( 'Enter Your Copyright info', 'silverbird' ),
            'section'     => 'silverbird_footer_section',
            'priority'    => 10,
) );

/** Footer End **/


/** Theme Color **/
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'color',
            'settings'    => 'header_title_color',
            'label'       => esc_html__( 'Site Title', 'silverbird' ),
            'description' => esc_html__( 'Select color for the Site Title. Note: if you have logo uploaded, this will not take effect', 'silverbird' ),
            'section'     => 'silverbird_header_colors',
            'default'     => 'BC9F60',
            'priority'    => 10,
            'choices'     => array(
                'alpha' => true,
            ),

) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'color',
            'settings'    => 'header_txtcolor',
            'label'       => esc_html__( 'Site Description', 'silverbird' ),
            'description' => esc_html__( 'Select color for the site description under the Site Title', 'silverbird' ),
            'section'     => 'silverbird_header_colors',
            'default'     => '000000',
            'priority'    => 10,
            'choices'     => array(
                'alpha' => true,
            ),

) );

WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'color',
            'settings'    => 'background_color',
            'label'       => esc_html__( 'Background Color', 'silverbird' ),
            'description' => esc_html__( 'Note: when you upload background image, this color will not take effect', 'silverbird' ),
            'section'     => 'silverbird_main_colors',
            'default'     => '',
            'priority'    => 10,
            'choices'     => array(
                'alpha' => true,
            ),

) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'color',
            'settings'    => 'silverbird_default_color',
            'label'       => esc_html__( 'Site Main Color', 'silverbird' ),
            'description' => esc_html__( 'Select site wide main color', 'silverbird' ),
            'section'     => 'silverbird_main_colors',
            'default'     => 'BC9F60',
            'priority'    => 10,
            'choices'     => array(
                'alpha' => true,
            ),

) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'color',
            'settings'    => 'silverbird_button_colors',
            'label'       => esc_html__( 'Button Color', 'silverbird' ),
            'description' => esc_html__( 'Select color for the site wide buttons', 'silverbird' ),
            'section'     => 'silverbird_main_colors',
            'default'     => 'BC9F60',
            'priority'    => 10,
            'choices'     => array(
                'alpha' => true,
            ),

) );
WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'color',
            'settings'    => 'silverbird_button_hover_colors',
            'label'       => esc_html__( 'Button Hover Color', 'silverbird' ),
            'description' => esc_html__( 'Select color for the site wide button hover', 'silverbird' ),
            'section'     => 'silverbird_main_colors',
            'default'     => 'BF9232',
            'priority'    => 10,
            'choices'     => array(
                'alpha' => true,
            ),

) );


WP3_Customizer::add_field(
    'silverbird-wp3',
        array(
            'type'        => 'radio-buttonset',
            'settings'    => 'silverbird_social_media_colors',
            'label'       => esc_html__( 'Social Media Icon Colors', 'silverbird' ),
            'description' => esc_html__( 'Select if you want the social media icons to be grey or with their default colors', 'silverbird' ),
            'section'     => 'silverbird_socialmedia_colors',
            'default'     => 'grey',
            'priority'    => 10,
            'choices'     => array(
                'coloured'  => esc_html__( 'Coloured', 'silverbird' ),
                'grey'      => esc_html__( 'Grey', 'silverbird' ),
            ),
) );
