<?php
/**
 * Customizer Theme Options Sections
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */


WP3_Customizer::add_section(
    'silverbird_general_section',
        array(
            'title'          => esc_html__( 'General Settings', 'silverbird' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 2,
            'icon'           => 'dashicons-admin-tools',

) );
WP3_Customizer::add_section(
    'silverbird_header_section',
        array(
            'title'          => esc_html__( 'Header & Menu', 'silverbird' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 2,
            'icon'           => 'dashicons-schedule',

) );
WP3_Customizer::add_section(
    'silverbird_slider_section',
        array(
            'title'          => esc_html__( 'Home Slider Section', 'silverbird' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 2,
            'icon'           => 'dashicons-align-center',

) );

WP3_Customizer::add_section(
    'silverbird_promo_section',
        array(
            'title'          => esc_html__( 'Home Promo Box', 'silverbird' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 2,
            'icon'           => 'dashicons-editor-insertmore',

) );

WP3_Customizer::add_section(
    'silverbird_posts_settings',
        array(
            'title'          => esc_html__( 'Posts & Archive Settings', 'silverbird' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 4,
            'icon'           => 'dashicons-admin-comments',

) );
WP3_Customizer::add_section(
    'silverbird_single_post_setting',
        array(
            'title'          => esc_html__( 'Single Post View', 'silverbird' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 5,
            'icon'           => 'dashicons-admin-page',

) );
WP3_Customizer::add_section(
    'silverbird_related_post_setting',
        array(
            'title'          => esc_html__( 'Related Posts', 'silverbird' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 5,
            'icon'           => 'dashicons-slides',

) );
WP3_Customizer::add_section(
    'silverbird_social_media',
        array(
            'title'          => esc_html__( 'Social Media Links', 'silverbird' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 6,
            'icon'           => 'dashicons-groups',

) );

/* Colors */
WP3_Customizer::add_section(
    'silverbird_header_colors',
        array(
            'title'          => esc_html__( 'Header Colors', 'silverbird' ),
            'panel'          => 'colors', // Not typically needed.
            'priority'       => 6,
            'icon'           => 'dashicons-archive',

) );
WP3_Customizer::add_section(
    'silverbird_main_colors',
        array(
            'title'          => esc_html__( 'Default Colors', 'silverbird' ),
            'panel'          => 'colors', // Not typically needed.
            'priority'       => 6,
            'icon'           => 'dashicons-laptop',

) );
WP3_Customizer::add_section(
    'silverbird_socialmedia_colors',
        array(
            'title'          => esc_html__( 'Social Media', 'silverbird' ),
            'panel'          => 'colors', // Not typically needed.
            'priority'       => 6,
            'icon'           => 'dashicons-groups',

) );

/* Foooter */
WP3_Customizer::add_section(
    'silverbird_footer_section',
        array(
            'title'          => esc_html__( 'Footer', 'silverbird' ),
            'panel'          => 'theme_options', // Not typically needed.
            'priority'       => 6,
            'icon'           => 'dashicons-screenoptions',

) );
