<?php
/**
 * Customizer Theme Options Panel
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

WP3_Customizer::add_panel(
	'theme_options',
		array(
    		'priority'    => 2,
    		'title'       => esc_html__( 'SilverBird Lite Options', 'silverbird' ),
    		'description' => esc_html__( 'This panel will provide all the options of the Theme.', 'silverbird' ),
    		'icon'        => 'dashicons-layout',
) );
WP3_Customizer::add_panel(
	'colors',
		array(
    		'priority'    => 2,
    		'title'       => esc_html__( 'Colors', 'silverbird' ),
    		'description' => esc_html__( 'This panel provides all the options for the Theme Colors.', 'silverbird' ),
    		'icon'        => 'dashicons-admin-appearance',
) );

?>
