<?php
/**
 * Silverbird Scripts
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */


/**
 * Enqueue scripts and styles.
 */
function silverbird_scripts() {

	wp_enqueue_style( 'bootstrap', SILVERBIRD_DIRECTORY_URI . '/assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'font-awesome', SILVERBIRD_DIRECTORY_URI .'/assets/css/font-awesome.min.css' );

	/*Animate Style*/
	wp_enqueue_style( 'animate', SILVERBIRD_DIRECTORY_URI . '/assets/css/animate.min.css' );
	wp_enqueue_style( 'owl-carousel', SILVERBIRD_DIRECTORY_URI . '/assets/css/owl.carousel.css' );
	wp_enqueue_style( 'owl-theme', SILVERBIRD_DIRECTORY_URI . '/assets/css/owl.theme.css' );
	wp_enqueue_style( 'owl-transitions', SILVERBIRD_DIRECTORY_URI . '/assets/css/owl.transitions.css' );
	wp_enqueue_style( 'slicknav', SILVERBIRD_DIRECTORY_URI . '/assets/css/slicknav.css' );

	wp_enqueue_style( 'silverbird-style', get_stylesheet_uri() );
	wp_enqueue_style( 'silverbird-responsive', SILVERBIRD_DIRECTORY_URI . '/assets/css/responsive.css' );

	wp_enqueue_script( 'bootstrap', SILVERBIRD_DIRECTORY_URI . '/assets/js/bootstrap.min.js', array(), '', true );
	wp_enqueue_script( 'owl-carousel', SILVERBIRD_DIRECTORY_URI . '/assets/js/owl.carousel.min.js', array(), '', true );
	wp_enqueue_script( 'imagesloaded', true );
	wp_enqueue_script( 'jquery-easing', SILVERBIRD_DIRECTORY_URI . '/assets/js/jquery.easing.js', array(), '', true );
	wp_enqueue_script( 'jquery-fitvids', SILVERBIRD_DIRECTORY_URI . '/assets/js/jquery.fitvids.js', array(), '', true );
	wp_enqueue_script( 'jquery-stickit', SILVERBIRD_DIRECTORY_URI . '/assets/js/jquery.stickit.js', array(), '', true );
	wp_enqueue_script( 'jquery-slicknav', SILVERBIRD_DIRECTORY_URI . '/assets/js/jquery.slicknav.js', array(), '', true );
	wp_enqueue_script( 'silverbird-custom-js', SILVERBIRD_DIRECTORY_URI . '/assets/js/custom.js', array('jquery'), '', true );


	wp_enqueue_script( 'silverbird-navigation', SILVERBIRD_DIRECTORY_URI . '/assets/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'silverbird-skip-link-focus-fix', SILVERBIRD_DIRECTORY_URI . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'silverbird_scripts' );

// Add Customizer Icon Style
function silverbird_custom_customize_css() {
	wp_enqueue_style( 'silverbird-customizer-style', SILVERBIRD_DIRECTORY_URI . '/core/customizer/style.css' );
}
add_action( 'customize_controls_enqueue_scripts', 'silverbird_custom_customize_css' );
