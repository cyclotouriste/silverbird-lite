/* ---------------------------------------------
 common scripts
 --------------------------------------------- */
;(function ($) {
    'use strict'; // use strict to start

    /* === Sticky Sidebar === */
    (function () {
        $("[data-sticky_column]").stickit({
            scope: StickScope.Parent,
            top: 0
        });
    }());


    /*=== Home Slider carousel =====*/
    (function () {
        $('.home-carousel').owlCarousel({
            singleItem: true,
            navigation: true,
            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            autoPlay: true,
            pagination: true,
            transitionStyle: "fade"
        });
    }());

    /*=== Related Post carousel =====*/
    (function () {
        $('.related-items').owlCarousel({
            items: 3,
            navigation: true,
            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            autoPlay: true,
            pagination: false
        });
    }());

    $('.slider-section').imagesLoaded(function () {
        $( ".slider-section" ).css( "display", "block" );
        $( ".promo-box" ).css( "display", "block" );

        /* === Gallery Format Slider === */
        (function () {
            $('.gallery-format-slider').owlCarousel({
                singleItem: true,
                navigation: true,
                navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                autoPlay: true,
                pagination: false,
                transitionStyle: "fade"
            });
        }());
   });

    /* === Back To Top === */
    (function () {
        $('.js-gotop').on('click', function(event){
            event.preventDefault();
            $('html, body').animate({scrollTop: 0}, 800);
            return false;
        });

        $(window).on('scroll', function(){
            var $win = $(window);
            if ($win.scrollTop() > 500) {
                $('.js-top').addClass('active');
            } else {
                $('.js-top').removeClass('active');
            }
        });
    }());



    /* === Fitvids js === */
    (function () {
        $(".entry-content").fitVids();
        $(".entry-video").fitVids();
        $(".entry-audio").fitVids();
    }());


    $(document).ready(function(){
        $('#menu').slicknav({
            prependTo:'#nav-menu'
        });

        /* === Header Search Btn === */
        $('#search-btn').on('click', function(){
            $('#search-overlay').css('display', 'block');
        });
        $('#menu-search-btn').on('click', function(){
            $('#search-overlay').css('display', 'block');
        });

        $('#closebtn').on('click', function(){
            $('#search-overlay').css('display', 'none');
        });
    });


    /* === Pre-loader === */

    $(document).ready(function(){
        $('.preloader').fadeOut();
        $('.spinner').delay(250).fadeOut('slow');
    });

})(jQuery);


