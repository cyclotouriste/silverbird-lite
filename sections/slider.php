<?php
/**
 * Silverbird Slider
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

if( get_theme_mod('silverbird_slides_section', false ) == true ) {

    $num = get_theme_mod('silverbird_slider_num', 4 );
    $orderby = get_theme_mod('silverbird_slides_orderby', 'date' );

    global $post;
    //Query slide Posts
     $args = array(
        'post_type'             => 'post',
        'post_status'           => 'publish',
        'posts_per_page'        => absint($num),
        'ignore_sticky_posts'   => true,
        'orderby'               => $orderby,
    );

    $slider = new WP_Query( $args );

if( $slider->have_posts() ) {

    $silverbird_img_siz = 'silverbird-home-slider';
    $silverbird_css_width = 'container';


    if ( get_theme_mod('silverbird_slides_width', 'centered' ) == 'fullwidth' ) {
        $silverbird_img_siz = 'silverbird-large';
        $silverbird_css_width = '';
    }


?>
   <!--slider section start-->
    <div class="slider-section">
        <div class="<?php echo esc_attr( $silverbird_css_width ); ?>">
            <div class="row">
                <div class="col-md-12">
                    <div class="home-carousel">
                        <?php while( $slider->have_posts() ) {
                            $slider->the_post(); ?>
                        <div class="home-slider">
                            <?php if( has_post_thumbnail() ){?>
                                <img src="<?php the_post_thumbnail_url($silverbird_img_siz); ?>" alt="<?php the_title_attribute();?>">
                            <?php } else { ?>
                                <img src="<?php echo esc_url( SILVERBIRD_DIRECTORY_URI ); ?>/assets/images/no-slider.jpg" alt="<?php the_title_attribute();?>">
                            <?php } ?>
                            <div class="slider-overlay text-center">
                                <?php the_title( '<h2 class="entry-title text-center">', '</h2>' );?>
                                <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn read-more text-uppercase">
                                    <?php esc_html_e('Read More','silverbird');?>
                                </a>
                            </div>
                        </div>
                        <?php }  wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--slider section end-->
    <?php } else { ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="nopost-message text-center">
                        <?php esc_html_e( '!!! Sorry. There are no posts in the slider.', 'silverbird' ); ?>
                    </p>
                </div>
            </div>
        </div>
<?php }

} // end if( get_theme_mod('silverbird_slides_section', false )

?>
