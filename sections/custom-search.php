<?php
/**
 * Custom Search
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */


?>

<div id="search-overlay" class="search-overlay">
    <span id="closebtn" title="Close Search"><i class="fa fa-times" aria-hidden="true"></i></span>
        <div class="overlay-content">
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                <input type="search" placeholder="<?php esc_html_e( 'Search...', 'silverbird' ); ?>" name="s">
                    <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
</div>
