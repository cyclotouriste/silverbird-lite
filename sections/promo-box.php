<?php
/**
 * Silverbird Promo Boxes
 *
 * @link https://github.com/MPolleke/silverbird-lite
 *
 * @package SilverBird Lite
 */

if( get_theme_mod('silverbird_promo_box', false ) == true ) {

    //Get the ID of the attached image
    $silverbird_one_img_id = get_theme_mod('silverbird_promo_one_image_url', '' );
    $silverbird_two_img_id = get_theme_mod('silverbird_promo_two_image_url', '' );
    $silverbird_three_img_id = get_theme_mod('silverbird_promo_three_image_url', '' );

    //Get the url of the attached image
    $silverbird_box1 = wp_get_attachment_image_src( $silverbird_one_img_id, 'silverbird-thumbnail' );
    $silverbird_box2 = wp_get_attachment_image_src( $silverbird_two_img_id, 'silverbird-thumbnail' );
    $silverbird_box3 = wp_get_attachment_image_src( $silverbird_three_img_id, 'silverbird-thumbnail' );

?>
    <!--promo box start-->
    <div class="promo-box text-uppercase text-center">
        <div class="container">
            <div class="row">
            <?php if (get_theme_mod('silverbird_promo_one_title', '' ) != '' || get_theme_mod('silverbird_promo_one_image_url', '' ) != '') { ?>
                <div class="col-md-4">
                    <div class="single-promo-box">
                        <?php if(get_theme_mod('silverbird_promo_one_url', '' ) !='') {?><a href="<?php echo esc_url( get_theme_mod('silverbird_promo_one_url', '' ) ); ?>">  <?php }?>
                            <?php if( get_theme_mod('silverbird_promo_one_image_url', '' ) != '' ){ ?>
                                <img src="<?php echo esc_url($silverbird_box1[0]); ?>" alt="<?php echo get_theme_mod('silverbird_promo_one_title', '' ); ?>">
                            <?php } else { ?>
                                <img src="<?php echo SILVERBIRD_DIRECTORY_URI; ?>/assets/images/promo-box.jpg" alt="<?php echo get_theme_mod('silverbird_promo_one_title', '' ); ?>">
                            <?php } ?>
                            <div class="overlay">
                                <?php if( get_theme_mod('silverbird_promo_one_title', '' ) !='') { ?>
                                <h3 class="promo-title">
                                    <span><?php echo get_theme_mod('silverbird_promo_one_title', '' ); ?></span>
                                </h3>
                               <?php } ?>
                            </div>
                        <?php if(get_theme_mod('silverbird_promo_one_url', '' ) !='') {?> </a><?php } ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (get_theme_mod('silverbird_promo_two_title', '' ) != '' || get_theme_mod('silverbird_promo_two_image_url', '' ) != '') { ?>
                <div class="col-md-4">
                    <div class="single-promo-box">
                        <?php if(get_theme_mod('silverbird_promo_two_url', '' ) !='') {?><a href="<?php echo esc_url( get_theme_mod('silverbird_promo_two_url', '' ) ); ?>">  <?php }?>
                            <?php if( get_theme_mod('silverbird_promo_two_image_url', '' ) != '' ){ ?>
                                <img src="<?php echo esc_url($silverbird_box2[0]); ?>" alt="<?php echo get_theme_mod('silverbird_promo_two_title', '' ); ?>">
                            <?php } else { ?>
                                <img src="<?php echo SILVERBIRD_DIRECTORY_URI; ?>/assets/images/promo-box.jpg" alt="<?php echo get_theme_mod('silverbird_promo_two_title', '' ); ?>">
                            <?php } ?>
                            <div class="overlay">
                                <?php if( get_theme_mod('silverbird_promo_two_title', '' ) !='') { ?>
                                <h3 class="promo-title">
                                    <span><?php echo get_theme_mod('silverbird_promo_two_title', '' ); ?></span>
                                </h3>
                               <?php } ?>
                            </div>
                        <?php if(get_theme_mod('silverbird_promo_two_url', '' ) !='') {?> </a><?php } ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (get_theme_mod('silverbird_promo_three_title', '' ) != '' || get_theme_mod('silverbird_promo_three_image_url', '' ) != '') { ?>
                <div class="col-md-4">
                    <div class="single-promo-box">
                        <?php if(get_theme_mod('silverbird_promo_three_url', '' ) !='') {?><a href="<?php echo esc_url( get_theme_mod('silverbird_promo_three_url', '' ) ); ?>">  <?php }?>
                            <?php if( get_theme_mod('silverbird_promo_three_image_url', '' ) != '' ){ ?>
                                <img src="<?php echo esc_url($silverbird_box3[0]); ?>" alt="<?php echo get_theme_mod('silverbird_promo_three_title', '' ); ?>">
                            <?php } else { ?>
                                <img src="<?php echo SILVERBIRD_DIRECTORY_URI; ?>/assets/images/promo-box.jpg" alt="<?php echo get_theme_mod('silverbird_promo_three_title', '' ); ?>">
                            <?php } ?>
                            <div class="overlay">
                                <?php if( get_theme_mod('silverbird_promo_three_title', '' ) !='') { ?>
                                <h3 class="promo-title">
                                    <span><?php echo get_theme_mod('silverbird_promo_three_title', '' ); ?></span>
                                </h3>
                               <?php } ?>
                            </div>
                        <?php if(get_theme_mod('silverbird_promo_three_url', '' ) !='') {?> </a><?php } ?>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
    <!--promo box end-->
    <?php
    wp_reset_postdata();
  ?>
<?php } ?>
